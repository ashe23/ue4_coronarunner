// Fill out your copyright notice in the Description page of Project Settings.


// Custom Headers
#include "TileManager.h"
#include "TileBase.h"

// Engine Headers
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Components/ArrowComponent.h"
#include "Components/SplineComponent.h"

DEFINE_LOG_CATEGORY(LogTileManager);

// Sets default values
ATileManager::ATileManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Active = nullptr;
	Prev = nullptr;
	Next = nullptr;
	Last = nullptr;
	SpawnedTilesAmount = 0;

}

// Called when the game starts or when spawned
void ATileManager::BeginPlay()
{
	Super::BeginPlay();
		
	this->Init();
}

// Called every frame
void ATileManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATileManager::SpawnTile()
{
	auto NewTile = Cast<ATileBase>(GetWorld()->SpawnActor(TileClass, &Last->NextSpawnPoint->GetComponentTransform()));
	if (!NewTile) return;

	NewTile->TileNum = ++SpawnedTilesAmount;
	Last = NewTile;

	this->ConnectTiles(Next, Last);
}

void ATileManager::SpawnInitialTile()
{
	auto NewTile = Cast<ATileBase>(GetWorld()->SpawnActor(TileClass, &Last->NextSpawnPoint->GetComponentTransform()));
	if (!NewTile) return;
	NewTile->TileNum = ++SpawnedTilesAmount;
	
	Next = NewTile;
	Last = NewTile;

	this->ConnectTiles(Active, Next);

}

void ATileManager::RemoveTile(ATileBase * Tile)
{
}

void ATileManager::SetActiveTile()
{
	Prev = Active;
	Active = Next;
	Next = Last;
}

void ATileManager::ConnectTiles(ATileBase * Tile1, ATileBase * Tile2)
{
	if (!Tile1 || !Tile2) return;

	auto ActiveTilePointsNum = Tile1->Lane->GetNumberOfSplinePoints() - 1;

	auto LastTileFirstPointLocation = Tile2->Lane->GetLocationAtSplinePoint(0, ESplineCoordinateSpace::World);
	auto LastTileFirstPointTanget = Tile2->Lane->GetTangentAtSplinePoint(0, ESplineCoordinateSpace::World);
	auto LastTileFirstPointUpVector = Tile2->Lane->GetUpVectorAtSplinePoint(0, ESplineCoordinateSpace::World);

	// Location
	Tile1->Lane->SetLocationAtSplinePoint(ActiveTilePointsNum, LastTileFirstPointLocation, ESplineCoordinateSpace::World);
	// Tangents
	Tile1->Lane->SetTangentAtSplinePoint(ActiveTilePointsNum, LastTileFirstPointTanget, ESplineCoordinateSpace::World);
	// UP Vector
	Tile1->Lane->SetUpVectorAtSplinePoint(ActiveTilePointsNum, LastTileFirstPointUpVector, ESplineCoordinateSpace::World);
}

void ATileManager::Init()
{
	// Finding First Tile Actor
	auto World = GetWorld();
	check(World && TEXT("World not found!"));

	auto FirstTile = UGameplayStatics::GetActorOfClass(World, TileClass);
	if (!FirstTile)
	{
		return;
	}

	Active = Cast<ATileBase>(FirstTile);
	check(Active && TEXT("Couldnt find First Tile! Make sure Tile Actor is in level."))

	Active->TileNum = 0;
	Last = Active;

	// Spawning Next Tile
	SpawnInitialTile();
}

