// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ObstacleBase.generated.h"

class UStaticMeshComponent;
class UBoxComponent;
class USplineComponent;
class ACoronaGameMode;

UCLASS()
class UE4_CORONARUNNER_API AObstacleBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AObstacleBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	bool ObstacleOverlapped = false;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	UBoxComponent* BoxCollision;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	UStaticMeshComponent* SM_ObstacleMesh;

	UPROPERTY()
	USplineComponent* ActiveLane;

	UPROPERTY()
	USplineComponent* NextLane;

	UFUNCTION()
	void OnMeshBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float Speed;
public:
	void MoveCarBySpline(float DeltaTime);
	
	void StopCarMovement();
	bool CanMove;
	float Distance;
	int32 Offset;
private:
	UPROPERTY()
	ACoronaGameMode* GameModePtr;
};
