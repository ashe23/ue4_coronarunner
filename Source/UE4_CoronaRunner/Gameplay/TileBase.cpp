// Fill out your copyright notice in the Description page of Project Settings.


#include "TileBase.h"
#include "Components/ArrowComponent.h"
#include "Components/SceneComponent.h"
#include "Components/BoxComponent.h"
#include "Components/SplineComponent.h"
#include "Components/PrimitiveComponent.h"

#include "../Core/CoronaGameMode.h"
#include "../DoctorBase.h"
#include "PickupBase.h"
#include "ObstacleBase.h"
#include "TileManager.h"

// Sets default values
ATileBase::ATileBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	TileType = ETileType::Basic;

	RootScene = CreateDefaultSubobject<USceneComponent>(TEXT("RootScene"));
	RootComponent = RootScene;

	NextSpawnPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("NextSpawnPoint"));
	NextSpawnPoint->AttachToComponent(RootScene, FAttachmentTransformRules::KeepRelativeTransform);

	NextTileSpawnTriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("NextSpawnPointTriggerBox"));
	NextTileSpawnTriggerBox->AttachToComponent(RootScene, FAttachmentTransformRules::KeepRelativeTransform);
	NextTileSpawnTriggerBox->OnComponentBeginOverlap.AddDynamic(this, &ATileBase::OnSpawnPointBoxOverlap);

	TileEndTriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("TileEndTriggerBox"));
	TileEndTriggerBox->AttachToComponent(RootScene, FAttachmentTransformRules::KeepRelativeTransform);
	TileEndTriggerBox->OnComponentBeginOverlap.AddDynamic(this, &ATileBase::OnTileEndBoxOverlap);


	Lane = CreateDefaultSubobject<USplineComponent>(TEXT("Lane"));
	Lane->AttachToComponent(RootScene, FAttachmentTransformRules::KeepRelativeTransform);

	SpawnDistanceOffset = 600.0f;
	SpawnDistance = 0.0f;
}

// Called when the game starts or when spawned
void ATileBase::BeginPlay()
{
	Super::BeginPlay();

	check(GetWorld());

	GameModePtr = Cast<ACoronaGameMode>(GetWorld()->GetAuthGameMode());

	this->SpawnPickups();
}

// Called every frame
void ATileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATileBase::OnSpawnPointBoxOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!OtherActor) return;
	if (!Cast<ADoctorBase>(OtherActor)) return;

	if (!Overlapped)
	{
		Overlapped = true;
		check(GameModePtr && "Game Mode is Nullptr!");
		GameModePtr->TileManager->SpawnTile();
		UE_LOG(LogTemp, Warning, TEXT("Spawn Point Begin Overlap"));
	}
}

void ATileBase::OnTileEndBoxOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!OtherActor) return;
	if (!Cast<ADoctorBase>(OtherActor)) return;

	if (!TileEnded)
	{
		TileEnded = true;
		check(GameModePtr && "Game Mode is Nullptr!");
		GameModePtr->TileManager->SetActiveTile();


		SpawnDistance = 0.0f;
		SetLifeSpan(3.0f);
	}
}

void ATileBase::SpawnPickups()
{
	check(GetWorld() && "No World!");
	if (!PickupClasses.Num())
	{
		UE_LOG(LogTemp, Warning, TEXT("No Pickup classes specified!"));
		return;
	}

	// if its first tile , move spawn distance a little bit
	if (TileNum == 0) SpawnDistance = 2500.0f;

	auto SplinePointsNum = Lane->GetNumberOfSplinePoints() - 1;
	auto SplineMaxLength = Lane->GetDistanceAlongSplineAtSplinePoint(SplinePointsNum);

	UE_LOG(LogTemp, Warning, TEXT("Spline Max Length: %f"), SplineMaxLength);

	while (SpawnDistance < SplineMaxLength)
	{
		float SpawnLength = FMath::RandRange(1500.0f, 4000.0f);
		if (SpawnDistance + SpawnLength >= SplineMaxLength)
		{
			SpawnDistance += SpawnDistanceOffset;
			continue;
		}

		const int32 NumPickups = SpawnLength / SpawnDistanceOffset;
		TSubclassOf<APickupBase> PickupClass;
		if (PickupClasses.Num() > 2)
		{
			PickupClass = FMath::RandBool() ? PickupClasses[0] : PickupClasses[1];
		}
		else
		{
			PickupClass = PickupClasses[0];
		}
		auto SpawnType = FMath::RandBool();

		if (SpawnType)
		{
			SpawnPickupsByNumLinear(NumPickups, PickupClass, 0.0f);
			SpawnPickupsByNumLinear(NumPickups, PickupClass, 200.0f);
			SpawnPickupsByNumLinear(NumPickups, PickupClass, -200.0f);
		}
		else
		{
			SpawnPickupsByNumCurved(NumPickups, PickupClass, GetRandomLaneOffset());
		}

		SpawnDistance += SpawnLength;
	}
}

void ATileBase::SpawnPickupsByNumCurved(int32 Amount, TSubclassOf<APickupBase>& PickupClass, float Laneoffset)
{
	float zOffset = 70.f;
	FTransform Transform = Lane->GetTransformAtDistanceAlongSpline(SpawnDistance, ESplineCoordinateSpace::World);
	FActorSpawnParameters ActorSpawnParams;
	ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	for (int32 i = 0; i < Amount; ++i)
	{
		Transform = Lane->GetTransformAtDistanceAlongSpline(SpawnDistance, ESplineCoordinateSpace::World);
		auto RightVector = Lane->GetRightVectorAtDistanceAlongSpline(SpawnDistance, ESplineCoordinateSpace::World);
		Transform.SetLocation(Transform.GetLocation() + FVector{ 0.0f , 0.0f , zOffset } + RightVector * Laneoffset);

		auto SpawnedPickup = GetWorld()->SpawnActor(PickupClass, &Transform, ActorSpawnParams);
		SpawnDistance += SpawnDistanceOffset;
		if (i >= Amount / 2)
		{
			zOffset -= 30.0f;
		}
		else
		{
			zOffset += 30.0f;
		}
	}
}

void ATileBase::SpawnPickupsByNumLinear(int32 Amount, TSubclassOf<APickupBase>& PickupClass, float Laneoffset)
{
	float LocalSpawnDistance = SpawnDistance;
	FTransform Transform = Lane->GetTransformAtDistanceAlongSpline(LocalSpawnDistance, ESplineCoordinateSpace::World);
	FActorSpawnParameters ActorSpawnParams;
	ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	for (int32 i = 0; i < Amount; ++i)
	{
		// Calculating transform on spline where to spawn pickup
		Transform = Lane->GetTransformAtDistanceAlongSpline(LocalSpawnDistance, ESplineCoordinateSpace::World);
		auto RightVector = Lane->GetRightVectorAtDistanceAlongSpline(LocalSpawnDistance, ESplineCoordinateSpace::World);
		Transform.SetLocation(Transform.GetLocation() + FVector{ 0.0f , 0.0f , 100.f } + RightVector * Laneoffset);

		// Spawning pickup
		auto SpawnedPickup = GetWorld()->SpawnActor(PickupClass, &Transform, ActorSpawnParams);
		LocalSpawnDistance += SpawnDistanceOffset;
	}
}

float ATileBase::GetRandomLaneOffset()
{
	auto RandInt = FMath::RandRange((int32)0, 2);
	auto offset = 0.0f;
	switch (RandInt)
	{
	case 0:
		offset = 0.0f;
		break;
	case 1:
		offset = 200.0f;
		break;
	case 2:
		offset = -200.0f;
		break;
	}
	return offset;
}

void ATileBase::SpawnObstacles()
{
	if (ObstacleClasses.Num() == 0) return;

	float ObstacleSpawnDist = 9000.0f;
	FTransform Transform = Lane->GetTransformAtDistanceAlongSpline(ObstacleSpawnDist, ESplineCoordinateSpace::World);
	FActorSpawnParameters ActorSpawnParams;
	ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	auto offset = GetRandomLaneOffset();
	auto RightVector = Lane->GetRightVectorAtDistanceAlongSpline(SpawnDistance, ESplineCoordinateSpace::World);
	Transform.SetLocation(Transform.GetLocation() + FVector{ 0.0f , 0.0f , 100.f } + RightVector * offset); // todo remove magic number
	auto SpawnedObstacleCar = GetWorld()->SpawnActor(ObstacleClasses[0], &Transform, ActorSpawnParams);
	if (SpawnedObstacleCar)
	{
		auto CastedCar = Cast<AObstacleBase>(SpawnedObstacleCar);
		if (CastedCar)
		{
			CastedCar->ActiveLane = Lane;
			CastedCar->Distance = ObstacleSpawnDist;
			if (GameModePtr->TileManager->Prev)
			{
				CastedCar->NextLane = GameModePtr->TileManager->Prev->Lane;
			}
		}
	}

}
