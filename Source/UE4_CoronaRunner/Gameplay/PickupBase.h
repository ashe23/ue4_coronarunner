// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickupBase.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogPickup, Log, All);

class UStaticMeshComponent;
class USphereComponent;

UCLASS()
class UE4_CORONARUNNER_API APickupBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickupBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	bool EnteredZone = false;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Pickup")
	UStaticMeshComponent* PickupMesh;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Pickup")
	USphereComponent* SphereCollision;
	
	UFUNCTION()
	void OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void PickupPicked();
	virtual void PickupPicked_Implementation();
};
