// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Enums/EnumContainer.h"
#include "TileBase.generated.h"

class USceneComponent;
class UArrowComponent;
class UBoxComponent;
class USplineComponent;
class ACoronaGameMode;
class APickupBase;
class AObstacleBase;


UCLASS()
class UE4_CORONARUNNER_API ATileBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATileBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USceneComponent* RootScene;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UArrowComponent* NextSpawnPoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UBoxComponent* NextTileSpawnTriggerBox;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UBoxComponent* TileEndTriggerBox;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USplineComponent* Lane;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Tile")
	int32 TileNum;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile")
	TEnumAsByte<ETileType::Type> TileType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile")
	TArray<TSubclassOf<APickupBase>> PickupClasses;	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile")
	TArray<TSubclassOf<AObstacleBase>> ObstacleClasses;
public:

	UFUNCTION()
	void OnSpawnPointBoxOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	UFUNCTION()
	void OnTileEndBoxOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

private:
	UPROPERTY()
	ACoronaGameMode* GameModePtr;
	bool Overlapped = false;
	bool TileEnded = false;

	// Pickups
	void SpawnPickups();
	float SpawnDistance;
	void SpawnPickupsByNumCurved(int32 Amount, TSubclassOf<APickupBase>& PickupClass, float Laneoffset);
	void SpawnPickupsByNumLinear(int32 Amount, TSubclassOf<APickupBase>& PickupClass, float Laneoffset);
	float GetRandomLaneOffset();

	// Obstacles
	void SpawnObstacles();

public:
	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	float SpawnDistanceOffset;
};
