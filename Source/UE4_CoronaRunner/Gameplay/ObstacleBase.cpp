// Fill out your copyright notice in the Description page of Project Settings.


#include "ObstacleBase.h"
#include "../Core/CoronaGameMode.h"
#include "TileManager.h"
#include "TileBase.h"
#include "../DoctorBase.h"

// Engine Headers
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Components/SplineComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/World.h"

// Sets default values
AObstacleBase::AObstacleBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ObstacleOverlapped = false;
	Speed = 10.f;
	Offset = 0;
	CanMove = true;

	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	RootComponent = BoxCollision;

	SM_ObstacleMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SM_ObstacleMesh"));
	SM_ObstacleMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	BoxCollision->OnComponentBeginOverlap.AddDynamic(this, &AObstacleBase::OnMeshBeginOverlap);
}

// Called when the game starts or when spawned
void AObstacleBase::BeginPlay()
{
	Super::BeginPlay();
	
	if (!GetWorld()) return;

	GameModePtr = Cast<ACoronaGameMode>(GetWorld()->GetAuthGameMode());
}

// Called every frame
void AObstacleBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// move car along spline
	this->MoveCarBySpline(DeltaTime);
}

void AObstacleBase::OnMeshBeginOverlap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (!OtherActor) return;
	if (!Cast<ADoctorBase>(OtherActor)) return;

	auto Pawn = Cast<ADoctorBase>(OtherActor);

	if (!ObstacleOverlapped)
	{
		ObstacleOverlapped = true;
		Pawn->StopPawnAnyMovement();
		//StopCarMovement();
		GameModePtr->GameOver();
		UE_LOG(LogTemp, Warning, TEXT("Game Over!"));
	}
}

void AObstacleBase::MoveCarBySpline(float DeltaTime)
{
	if (!CanMove) return;
	if (!ActiveLane) return;

	if (Distance < 0)
	{
		if (NextLane)
		{
			ActiveLane = NextLane;
			Distance = ActiveLane->GetSplineLength();
		}
	}
	else
	{
		auto Transform = ActiveLane->GetTransformAtDistanceAlongSpline(Distance, ESplineCoordinateSpace::World);
		auto RightVector = ActiveLane->GetRightVectorAtDistanceAlongSpline(Distance, ESplineCoordinateSpace::World);

		Transform.SetLocation(Transform.GetLocation() + FVector{ 0.0, 0.0, 50.0f } + RightVector * Offset * 200.0f);
		SetActorTransform(Transform);

		Distance -= Speed * DeltaTime * 100.0f;
		//UE_LOG(LogTemp, Warning, TEXT("Distance: %f"), Distance);
	}

}

void AObstacleBase::StopCarMovement()
{
	CanMove = false;
}

