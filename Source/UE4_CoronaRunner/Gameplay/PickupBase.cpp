// Fill out your copyright notice in the Description page of Project Settings.



#include "PickupBase.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "../DoctorBase.h"

DEFINE_LOG_CATEGORY(LogPickup);

// Sets default values
APickupBase::APickupBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SpehereCollision"));
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &APickupBase::OnComponentBeginOverlap);

	RootComponent = SphereCollision;

	PickupMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PickupMesh"));
	PickupMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void APickupBase::BeginPlay()
{
	Super::BeginPlay();
		
}

// Called every frame
void APickupBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickupBase::OnComponentBeginOverlap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (!OtherActor) return;
	if (EnteredZone) return;
	if (!Cast<ADoctorBase>(OtherActor)) return;

	EnteredZone = true;

	this->PickupPicked();
}

void APickupBase::PickupPicked_Implementation()
{
	UE_LOG(LogPickup, Log, TEXT("PickupPicked Event Called!"));	
}

