// Fill out your copyright notice in the Description page of Project Settings.

// Custom Headers
#include "PickupManager.h"
#include "TileBase.h"
#include "PickupBase.h"

// Engine Headers
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SplineComponent.h"

// Sets default values
APickupManager::APickupManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Line = CreateDefaultSubobject<USplineComponent>(TEXT("Line"));
	RootComponent = Line;

}

// Called when the game starts or when spawned
void APickupManager::BeginPlay()
{
	Super::BeginPlay();
	
	this->GeneratePickups();
}

// Called every frame
void APickupManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickupManager::GeneratePickups()
{
	// Spawning PickupClass Actors along Line(Spline)
	float SplineLength = Line->GetSplineLength();
	float Dist = 0.0f;
	float Offset = SplineLength / Amount;

	while (Dist < SplineLength)
	{
		FTransform Transform = Line->GetTransformAtDistanceAlongSpline(Dist, ESplineCoordinateSpace::World);
		this->SpawnPickup(Transform);
		Dist += Offset;
	}

}

void APickupManager::SpawnPickup(FTransform Transform)
{
	if (!PickupClass) return;
	auto World = GetWorld();

	check(World && TEXT("World not found!"));

	GetWorld()->SpawnActor(PickupClass, &Transform);
}

