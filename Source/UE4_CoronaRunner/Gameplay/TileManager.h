// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TileManager.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogTileManager, Log, All);

class ATileBase;

UCLASS()
class UE4_CORONARUNNER_API ATileManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATileManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	UPROPERTY()
	ATileBase* Active;
	UPROPERTY()
	ATileBase* Prev;
	UPROPERTY()
	ATileBase* Next;
	UPROPERTY()
	ATileBase* Last;

private:
	// Finds first tile actor in level, and spawn next one
	void Init();
	// Connects splines of given tiles
	void ConnectTiles(ATileBase* Tile1, ATileBase* Tile2);

	int32 SpawnedTilesAmount;
public:
	void SpawnTile();
	void SpawnInitialTile();
	void RemoveTile(ATileBase* Tile);
	void SetActiveTile();
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tile Manager")
	TSubclassOf<ATileBase> TileClass;

};
