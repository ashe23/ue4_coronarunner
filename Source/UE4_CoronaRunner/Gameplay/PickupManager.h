// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickupManager.generated.h"

class APickupBase;
class USplineComponent;

UCLASS()
class UE4_CORONARUNNER_API APickupManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickupManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "PickupManagerSettings")
	int32 Amount;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "PickupManagerSettings")
	TSubclassOf<APickupBase> PickupClass; // todo maybe add pickup variations, or just create differnt managers

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	USplineComponent* Line;

	void GeneratePickups();

private:
	void SpawnPickup(FTransform Transform);

};
