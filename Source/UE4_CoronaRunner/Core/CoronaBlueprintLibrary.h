// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "CoronaBlueprintLibrary.generated.h"


class UWorld;
class USoundBase;

/**
 * 
 */
UCLASS()
class UE4_CORONARUNNER_API UCoronaBlueprintLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	/** Show the native loading screen, such as on a map transfer. If bPlayUntilStopped is false, it will be displayed for PlayTime and automatically stop */
	UFUNCTION(BlueprintCallable, Category = Loading)
	static void PlayLoadingScreen(bool bPlayUntilStopped, float PlayTime);

	UFUNCTION(BlueprintCallable, Category = Loading)
	static void StopLoadingScreen();

	UFUNCTION(BlueprintCallable, Category = Audio, meta = (WorldContext = "WorldContextObject"))
	static void PlaySound(const UObject* WorldContextObject, USoundBase* Sound, float Volume);

	UFUNCTION(BlueprintCallable, Category = Gameplay, meta = (WorldContext = "WorldContextObject"))
	static void Vibrate(const UObject* WorldContextObject, float Duration, float Intensity);
};
