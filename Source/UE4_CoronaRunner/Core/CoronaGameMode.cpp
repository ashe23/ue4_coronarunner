// Fill out your copyright notice in the Description page of Project Settings.


#include "CoronaGameMode.h"
#include "../DoctorBase.h"
#include "../Gameplay/TileBase.h"
#include "../Gameplay/TileManager.h"
#include "../Gameplay/ObstacleBase.h"
#include "CoronaSaveGame.h"

#include "CoronaRunnerLoadingScreen/Public/CoronaRunnerLoadingScreen.h"

// Engine Headers
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "Components/ArrowComponent.h"
#include "Components/SplineComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "TimerManager.h"


ACoronaGameMode::ACoronaGameMode()
{
	DefaultPawnClass = ADoctorBase::StaticClass();	

	GameStatus = ECoronaGameStatus::Idle;

	CarSpawnDelay = 5.0f;
	SpawnOffset = 10000.0f;
	StartGameDelay = 3;
	StartGameTimerValue = StartGameDelay;
}

void ACoronaGameMode::StartPlay()
{
	this->FindTileManager();

	Super::StartPlay();
	
	GetWorldTimerManager().SetTimer(StartGameTimerHandle, this, &ACoronaGameMode::StartGameTimerCountDown, 1.0f, true);	
}

#if WITH_EDITOR
void ACoronaGameMode::PostEditChangeProperty(FPropertyChangedEvent & PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	StartGameTimerValue = StartGameDelay;
}
#endif

void ACoronaGameMode::StartGame_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("Game Started!"));
	GameStatus = ECoronaGameStatus::Game;

	//FTimerHandle DummyTimerHandle;
	//GetWorldTimerManager().SetTimer(DummyTimerHandle, this, &ACoronaGameMode::SpawnMovableObstacle, CarSpawnDelay, true);
	FTimerHandle DummyTimerHandle1;
	GetWorldTimerManager().SetTimer(DummyTimerHandle1, this, &ACoronaGameMode::IncrementInfection, 2.0f, true);
}

void ACoronaGameMode::StartGameTimerCountDown_Implementation()
{
	if (--StartGameTimerValue == 0)
	{
		GetWorldTimerManager().ClearTimer(StartGameTimerHandle);
		this->StartGame();
	}

	UE_LOG(LogTemp, Warning, TEXT("TimerValue: %f"), StartGameTimerValue);
}

void ACoronaGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (GameStatus == ECoronaGameStatus::Game)
	{
		this->CalculateScore();
	}

	// if player fully infected => Game Over!
	if (GameStatus != ECoronaGameStatus::Gameover && ScoreData.InfectionBarValue >= 1.0f)
	{		
		this->GameOver();
	}
}

void ACoronaGameMode::FindTileManager()
{
	auto World = GetWorld();
	check(World && TEXT("Cant Find World!"));

	auto TileManagerTemp = UGameplayStatics::GetActorOfClass(World, TileManagerClass);
	if (!TileManagerTemp)
	{
		UE_LOG(LogTemp, Warning, TEXT("Cant Find TileManager! Make Sure there is one Tile Manager actor in level."));
		return;
	}

	TileManager = Cast<ATileManager>(TileManagerTemp);
	check(TileManager && TEXT("Something went wrong when casting Tile Manager!"));
}

void ACoronaGameMode::SpawnMovableObstacle()
{
	auto Pawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (!Pawn) return;
	auto CastedPawn = Cast<ADoctorBase>(Pawn);
	if (!CastedPawn) return;

	float CarSpawnDistance = CastedPawn->Distance + SpawnOffset;
	float CarAmount = FMath::RandRange((int32)1, (int32)3);

	FTransform Transform;
	Transform.SetLocation(FVector{ 0.0f });

	for (int32 i = 0; i < CarAmount; ++i)
	{
		auto SpawnedCar = Cast<AObstacleBase>(GetWorld()->SpawnActor(CarClass, &Transform));
		if (CarSpawnDistance > CastedPawn->SplineMaxDistance)
		{
			SpawnedCar->ActiveLane = CastedPawn->NextLane;
			SpawnedCar->NextLane = CastedPawn->ActiveLane;
			SpawnedCar->Distance = CarSpawnDistance - CastedPawn->SplineMaxDistance;		
		}
		else
		{
			SpawnedCar->ActiveLane = CastedPawn->ActiveLane;
			SpawnedCar->Distance = CarSpawnDistance;
		}

		SpawnedCar->Speed = FMath::RandRange(10.0f, 20.0f);
		SpawnedCar->Offset = FMath::GetMappedRangeValueClamped(FVector2D{ 0 , 2 }, FVector2D{ -1 , 1}, i); // Todo:ashe23 improve randomazation logic
	}
}

void ACoronaGameMode::IncrementInfection()
{
	if (GameStatus != ECoronaGameStatus::Gameover)
	{
		ScoreData.InfectionBarValue += ScoreData.InfectionBarTickValue;
		FMath::Clamp(ScoreData.InfectionBarValue, 0.0f, 1.0f);
		UE_LOG(LogTemp, Warning, TEXT("Infection: %f"), ScoreData.InfectionBarValue);
	}
}

void ACoronaGameMode::CalculateScore()
{
	ScoreData.TotalScore += ScoreData.ScoreIncrementAmount;	
}

void ACoronaGameMode::AddScore(int32 Score)
{
	ScoreData.TotalScore += Score;
	ScoreData.InfectionBarValue += ScoreData.InfectionBarPickupValue;
}

void ACoronaGameMode::AddPickupPicked(int32 Amount)
{
	ScoreData.PickupsPicked += Amount;
}

void ACoronaGameMode::GameOver_Implementation()
{
	if (GameStatus == ECoronaGameStatus::Gameover) return;

	GameStatus = ECoronaGameStatus::Gameover;
}
