// Fill out your copyright notice in the Description page of Project Settings.


#include "CoronaGameInstance.h"
#include "CoronaSaveGame.h"
#include "CoronaBlueprintLibrary.h"
#include "CoronaRunnerLoadingScreen/Public/CoronaRunnerLoadingScreen.h"

#include "MoviePlayer/Public/MoviePlayer.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Engine.h"

void UCoronaGameInstance::Init()
{
	SaveSlotName = TEXT("CoronaSaveSlot");
	PlayerSaveSlotIndex = 1;
	bPlayedMainMenuAnim = false;

	Super::Init();


	FCoreUObjectDelegates::PreLoadMap.AddUObject(this, &UCoronaGameInstance::BeginLoadingScreen);
	FCoreUObjectDelegates::PostLoadMapWithWorld.AddUObject(this, &UCoronaGameInstance::EndLoadingScreen);

	this->GetUserSaveData();
}

void UCoronaGameInstance::BeginLoadingScreen(const FString & MapName)
{
	if (!IsRunningDedicatedServer())
	{
		UCoronaBlueprintLibrary::PlayLoadingScreen(true, 3.0f);
	}
}

void UCoronaGameInstance::EndLoadingScreen(UWorld * InLoadedWorld)
{
	UCoronaBlueprintLibrary::StopLoadingScreen();
}

void UCoronaGameInstance::ResaveGameData(FUserSaveData SaveData)
{
	UCoronaSaveGame* SaveGameData = Cast<UCoronaSaveGame>(UGameplayStatics::LoadGameFromSlot(SaveSlotName, PlayerSaveSlotIndex));
	if (!SaveGameData) return;

	SaveGameData->bSound = SaveData.bSoundActive;
	SaveGameData->bVibration = SaveData.bVibrationActive;
	SaveGameData->MyScore = SaveData.MyScore;
	SaveGameData->BestScore = SaveData.BestScore;

	UGameplayStatics::AsyncSaveGameToSlot(SaveGameData, SaveSlotName, PlayerSaveSlotIndex);

	this->UpdateUserSaveData(SaveGameData);

	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage((int32)-1, 10.0f, FColor::Red, FString::Printf(TEXT("Settings Saved!")));
	}
}

void UCoronaGameInstance::ClearSaveGameData()
{
	UGameplayStatics::DeleteGameInSlot(SaveSlotName, 1);
}

bool UCoronaGameInstance::CanPlaySound() const
{
	return UserSaveData.bSoundActive;
}

bool UCoronaGameInstance::CanVibrate() const
{
	return UserSaveData.bVibrationActive;
}



void UCoronaGameInstance::GetUserSaveData()
{
	// first check if save data file already exists
	UCoronaSaveGame* CoronaSaveData = Cast<UCoronaSaveGame>(UGameplayStatics::LoadGameFromSlot(SaveSlotName, PlayerSaveSlotIndex));
	if (CoronaSaveData)
	{
		this->UpdateUserSaveData(CoronaSaveData);
	}
	else // if no save data file exists create new one with default values
	{
		UCoronaSaveGame* SaveGameData = Cast<UCoronaSaveGame>(UGameplayStatics::CreateSaveGameObject(UCoronaSaveGame::StaticClass()));

		if (!SaveGameData)
		{
			UE_LOG(LogTemp, Warning, TEXT("Cant Create save file!"));
			return;
		}

		UGameplayStatics::AsyncSaveGameToSlot(SaveGameData, SaveSlotName, PlayerSaveSlotIndex);

		this->UpdateUserSaveData(SaveGameData);
	}

}

void UCoronaGameInstance::UpdateUserSaveData(const UCoronaSaveGame* SaveGameData)
{
	UserSaveData.bSoundActive = SaveGameData->bSound;
	UserSaveData.bVibrationActive = SaveGameData->bVibration;
	UserSaveData.MyScore = SaveGameData->MyScore;
	UserSaveData.BestScore = SaveGameData->BestScore;
}


