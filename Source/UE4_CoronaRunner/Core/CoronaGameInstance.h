// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "../Structs/UserSaveData.h"
#include "CoronaGameInstance.generated.h"

class UUserWidget;
class UCoronaSaveGame;


/**
 * 
 */
UCLASS()
class UE4_CORONARUNNER_API UCoronaGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	virtual void Init() override;

	UFUNCTION()
	virtual void BeginLoadingScreen(const FString& MapName);
	UFUNCTION()
	virtual void EndLoadingScreen(UWorld* InLoadedWorld);

	UFUNCTION(BlueprintCallable)
	void ResaveGameData(FUserSaveData SaveData);
	UFUNCTION(BlueprintCallable)
	void ClearSaveGameData();

	UFUNCTION(BlueprintCallable)
	bool CanPlaySound() const;

	UFUNCTION(BlueprintCallable)
	bool CanVibrate() const;
	

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	FUserSaveData UserSaveData;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool bPlayedMainMenuAnim;


private:
	void GetUserSaveData();
	void UpdateUserSaveData(const UCoronaSaveGame* SaveGameData);
	
	FString SaveSlotName;
	uint32 PlayerSaveSlotIndex;
};
