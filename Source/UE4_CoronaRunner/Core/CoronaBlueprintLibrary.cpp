// Fill out your copyright notice in the Description page of Project Settings.


#include "CoronaBlueprintLibrary.h"
#include "CoronaRunnerLoadingScreen/Public/CoronaRunnerLoadingScreen.h"
#include "CoronaGameInstance.h"

#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "Runtime/Engine/Classes/Engine/World.h"

void UCoronaBlueprintLibrary::PlayLoadingScreen(bool bPlayUntilStopped, float PlayTime)
{
	ICoronaRunnerLoadingScreenModule& LoadingScreenModule = ICoronaRunnerLoadingScreenModule::Get();
	LoadingScreenModule.StartInGameLoadingScreen(bPlayUntilStopped, PlayTime);
}

void UCoronaBlueprintLibrary::StopLoadingScreen()
{
	ICoronaRunnerLoadingScreenModule& LoadingScreenModule = ICoronaRunnerLoadingScreenModule::Get();
	LoadingScreenModule.StopInGameLoadingScreen();
}

void UCoronaBlueprintLibrary::PlaySound(const UObject* WorldContextObject, USoundBase* Sound, float Volume = 1.0f)
{
	if (!WorldContextObject || !Sound) return;

	auto World = WorldContextObject->GetWorld();
	
	if (!World) return;

	auto GameInstance = Cast<UCoronaGameInstance>(UGameplayStatics::GetGameInstance(World));
	if (!GameInstance) return;

	if (GameInstance->CanPlaySound())
	{
		UGameplayStatics::PlaySound2D(World, Sound, Volume);
	}
}

void UCoronaBlueprintLibrary::Vibrate(const UObject* WorldContextObject, float Duration = 0.3f, float Intensity = 0.5f)
{
	if (!WorldContextObject) return;

	auto World = WorldContextObject->GetWorld();
	auto PlayerController = World->GetFirstPlayerController();
	if (!PlayerController) return;

	auto GameInstance = Cast<UCoronaGameInstance>(UGameplayStatics::GetGameInstance(World));
	if (!GameInstance) return;

	if (GameInstance->CanVibrate())
	{
		PlayerController->PlayDynamicForceFeedback(
			Intensity,
			Duration, 
			true, 
			true,
			true,
			true,
			EDynamicForceFeedbackAction::Start
		);
	}
}
