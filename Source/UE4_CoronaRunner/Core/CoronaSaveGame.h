// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "CoronaSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class UE4_CORONARUNNER_API UCoronaSaveGame : public USaveGame
{
	GENERATED_BODY()
public:
	UPROPERTY(VisibleAnywhere)
	bool bSound;

	UPROPERTY(VisibleAnywhere)
	bool bVibration;

	UPROPERTY(VisibleAnywhere)
	int32 MyScore;

	UPROPERTY(VisibleAnywhere)
	int32 BestScore;

	UPROPERTY(VisibleAnywhere)
	uint32 UserIndex;

	UPROPERTY(VisibleAnywhere)
	FString SaveSlotName;

	UCoronaSaveGame();
};
