// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Enums/EnumContainer.h"
#include "../Structs/ScoreData.h"
#include "GameFramework/GameModeBase.h"
#include "CoronaGameMode.generated.h"


class ATileBase;
class ATileManager;
class AObstacleBase;

/**
 * 
 */
UCLASS()
class UE4_CORONARUNNER_API ACoronaGameMode : public AGameModeBase
{
	GENERATED_BODY()
public:
	ACoronaGameMode();

	virtual void StartPlay() override;
#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Game Status")
	TEnumAsByte< ECoronaGameStatus::Type> GameStatus;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Game Status")
	int32 StartGameDelay;
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Game Status")
	int32 StartGameTimerValue;
	FTimerHandle StartGameTimerHandle;

	void Tick(float DeltaSeconds) override;

public:
	void FindTileManager();

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	FScoreData ScoreData;

	UPROPERTY()
	ATileManager* TileManager;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tile Manager")
	TSubclassOf<ATileManager> TileManagerClass;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "MovableObstacles")
	TSubclassOf<AObstacleBase> CarClass;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "MovableObstacles")
	float CarSpawnDelay;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "MovableObstacles")
	float SpawnOffset;

	void SpawnMovableObstacle();

	void IncrementInfection();

	UFUNCTION(BlueprintCallable)
	void CalculateScore();

	UFUNCTION(BlueprintCallable)
	void AddScore(int32 Score);

	UFUNCTION(BlueprintCallable)
	void AddPickupPicked(int32 Amount);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void StartGame();
	virtual void StartGame_Implementation();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Game Status")
	void StartGameTimerCountDown();
	virtual void StartGameTimerCountDown_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void GameOver();
	virtual void GameOver_Implementation();	


};
