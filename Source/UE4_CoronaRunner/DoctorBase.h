// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/TimelineComponent.h"
#include "DoctorBase.generated.h"


class USplineComponent;
class ACoronaGameMode;

UCLASS()
class UE4_CORONARUNNER_API ADoctorBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ADoctorBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	// ~ Lane logic start
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USplineComponent* ActiveLane;
	USplineComponent* NextLane;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Distance;
	float SplineMaxDistance;
	bool bChangingLane;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector Direction;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Offset;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ScaleValue;
public:
	void FindActiveLane();
	void SetPawnInitialLocation();
	void MovePawnBySpline(float DeltaSeconds);
	void CalculateDistanceTravelled(float DeltaSeconds);
	void CheckSplineEnd();
	void ClearDistances();

	UFUNCTION(BlueprintCallable)
	void ChangeLane(int32 LaneOffset);
	// ~ Lane logic end

	// ~ Lane Change start
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	float StartLoc;
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	float EndLoc;

	int32 LaneNumber = 0;
	// ~ Lane Change end

public:
	UPROPERTY()
	ACoronaGameMode* GameModePtr;

	void GetGameMode();

	UFUNCTION(BlueprintCallable)
	void StopPawnAnyMovement();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnPawnStopMovement();
	virtual void OnPawnStopMovement_Implementation();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCanMove;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float DistanceTravelled;

};
