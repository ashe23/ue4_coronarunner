#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "UserSaveData.generated.h"

USTRUCT(BlueprintType)
struct FUserSaveData
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool bSoundActive;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool bVibrationActive;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	int32 MyScore;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	int32 BestScore;

	FUserSaveData()
	{
		bSoundActive = true;
		bVibrationActive = true;
		MyScore = 0;
		BestScore = 0;
	}
};