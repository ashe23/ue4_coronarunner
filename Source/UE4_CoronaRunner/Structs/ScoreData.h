// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "ScoreData.generated.h"

USTRUCT(BlueprintType)
struct FScoreData
{
	GENERATED_BODY()
		
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 TotalScore;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float ScoreIncrementAmount;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float InfectionBarValue;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float InfectionBarTickValue;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float InfectionBarPickupValue;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 ComboMultiplier;	

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 PickupsPicked;

	FScoreData()
	{
		TotalScore = 0;
		ScoreIncrementAmount = 0.1f;
		ComboMultiplier = 1;
		InfectionBarValue = 0.0f;
		InfectionBarTickValue = 0.05f;
		InfectionBarPickupValue = -0.01f;
		PickupsPicked = 0;
	}
};