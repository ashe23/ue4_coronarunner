// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"

UENUM(BlueprintType)
namespace ECoronaGameStatus 
{
	enum Type
	{
		Idle,
		Game,
		Gameover
	};

}

UENUM(BlueprintType)
namespace ETileType
{
	enum Type
	{
		Basic,
		CrossRoad
	};
}