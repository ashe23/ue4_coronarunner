// Fill out your copyright notice in the Description page of Project Settings.


#include "DoctorBase.h"
#include "Core/CoronaGameMode.h"
#include "Gameplay/TileManager.h"
#include "Gameplay/TileBase.h"
#include "Components/InputComponent.h"
#include "Components/SplineComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/Controller.h"
#include "Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/CharacterMovementComponent.h"


// Sets default values
ADoctorBase::ADoctorBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	LaneNumber = 0;
	bChangingLane = false;
	Distance = 500.0f;
	SplineMaxDistance = 0.0f;

	Direction = FVector{ 0.0f, 0.0f ,0.0f };
	Offset = 0.0f;
	ScaleValue = 1.0f;
	bCanMove = false;
	DistanceTravelled = 0.0f;
}

// Called when the game starts or when spawned
void ADoctorBase::BeginPlay()
{
	Super::BeginPlay();

	GetGameMode();

	FindActiveLane();	
}

// Called every frame
void ADoctorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!GameModePtr) return;
	if (GameModePtr->GameStatus == ECoronaGameStatus::Game)
	{
		if (bCanMove)
		{
			AddMovementInput(Direction, ScaleValue);

			// Spline movement
			MovePawnBySpline(DeltaTime);
			CheckSplineEnd();
		}
	}

}

// Called to bind functionality to input
void ADoctorBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//PlayerInputComponent->BindTouch(IE_Pressed, this, &ADoctorBase::TouchStarted);
	//PlayerInputComponent->BindTouch(IE_Released, this, &ADoctorBase::TouchEnded);
}


void ADoctorBase::FindActiveLane()
{
	check(GameModePtr && TEXT("GameModePtr is null!"));
	check(GameModePtr->TileManager && TEXT("TileManager is null!"));

	if (!GameModePtr->TileManager->Active) return;


	ActiveLane = GameModePtr->TileManager->Active->Lane;

	int32 LastPoint = ActiveLane->GetNumberOfSplinePoints() - 1;
	SplineMaxDistance = ActiveLane->GetDistanceAlongSplineAtSplinePoint(LastPoint);

	// Placing pawn in start of spline
	SetPawnInitialLocation();

	// Setting next lane todo
	if (!GameModePtr->TileManager->Next) return;
	NextLane = GameModePtr->TileManager->Next->Lane;
}

void ADoctorBase::SetPawnInitialLocation()
{
	if (!ActiveLane) 
	{
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("Initial Distance: %f"), Distance);
	FTransform Transform = ActiveLane->GetTransformAtDistanceAlongSpline(Distance, ESplineCoordinateSpace::World);
	FVector NewLocation = FVector{ Transform.GetLocation().X, Transform.GetLocation().Y, GetActorLocation().Z};
	Transform.SetLocation(NewLocation);

	SetActorTransform(Transform);	
}

void ADoctorBase::MovePawnBySpline(float DeltaSeconds)
{
	if (bChangingLane) return;
	if (!ActiveLane) return;
	if (!ActiveLane->IsValidLowLevelFast()) return;
	//if (!NextLane) return;

	CalculateDistanceTravelled(DeltaSeconds);

	FRotator Rot = ActiveLane->GetRotationAtDistanceAlongSpline(Distance, ESplineCoordinateSpace::World);
	Rot.Roll = 0.0f;
	Rot.Pitch = 0.0f;

	FVector Loc = ActiveLane->GetLocationAtDistanceAlongSpline(Distance, ESplineCoordinateSpace::World);
	Loc.Z = GetActorLocation().Z;

	auto Location = ActiveLane->GetLocationAtDistanceAlongSpline(Distance, ESplineCoordinateSpace::World);
	auto RightVector = ActiveLane->GetRightVectorAtDistanceAlongSpline(Distance, ESplineCoordinateSpace::World);
	RightVector.Normalize();


	SetActorRotation(Rot);
	SetActorLocation(Loc + RightVector * Offset);
	Direction = ActiveLane->GetDirectionAtDistanceAlongSpline(Distance, ESplineCoordinateSpace::World);

	//GEngine->AddOnScreenDebugMessage((int32)-1, 0.0f, FColor::Red, FString::Printf(TEXT("Direction: %s"), *Direction.ToString()));
	//GEngine->AddOnScreenDebugMessage((int32)-1, 0.0f, FColor::Green, FString::Printf(TEXT("Offset: %f"), Offset));

	//DrawDebugSphere(GetWorld(), Location + RightVector * 200.0f, 10.0f, 8, FColor::Green, false, 0.0f);
	//DrawDebugSphere(GetWorld(), Location + RightVector * -200.0f, 10.0f, 8, FColor::Cyan, false, 0.0f);
	//DrawDebugDirectionalArrow(GetWorld(), Location, Location + RightVector * 50.0f, 10.0f, FColor::Blue, false, 0.0f);
	//DrawDebugDirectionalArrow(GetWorld(), Location, Location + RightVector * -50.0f, 10.0f, FColor::Emerald, false, 0.0f);
	//GEngine->AddOnScreenDebugMessage((int32)-1, 0.0f, FColor::Green, FString::Printf(TEXT("Location: %s"), *Location.ToString()));
}

void ADoctorBase::CalculateDistanceTravelled(float DeltaSeconds)
{
	Distance += DeltaSeconds * GetCharacterMovement()->MaxWalkSpeed;	
	Distance = FMath::Clamp(Distance, 0.0f, SplineMaxDistance);
	DistanceTravelled += DeltaSeconds * GetCharacterMovement()->MaxWalkSpeed / 1000.0f;

	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage((int32)-1, 0.0f, FColor::Red, FString::Printf(TEXT("Distance Travelled: %f"), DistanceTravelled));
	}
}

void ADoctorBase::CheckSplineEnd()
{
	if (!NextLane) return;

	if (SplineMaxDistance - Distance < 5.0f)
	{
		if (NextLane == ActiveLane)
		{
			ScaleValue = 0.0f;
			UE_LOG(LogTemp, Warning, TEXT("Stopping movement!"));
			return;
		}

		ActiveLane = NextLane;
		ClearDistances();

		NextLane = GameModePtr->TileManager->Next->Lane;
	}
}

void ADoctorBase::ClearDistances()
{
	Distance = 0.0f;
	auto PointNum = ActiveLane->GetNumberOfSplinePoints() - 1;
	SplineMaxDistance = ActiveLane->GetDistanceAlongSplineAtSplinePoint(PointNum);
}

void ADoctorBase::ChangeLane(int32 LaneOffset)
{
	LaneNumber += LaneOffset;
	LaneNumber = FMath::Clamp(LaneNumber, -1, 1);

	StartLoc = Offset;
	EndLoc = 200 * LaneNumber;
}

void ADoctorBase::GetGameMode()
{
	auto World = GetWorld();
	if (!World) return;
	if (!World->GetAuthGameMode()) return;
	GameModePtr = Cast<ACoronaGameMode>(GetWorld()->GetAuthGameMode());	
}

void ADoctorBase::StopPawnAnyMovement()
{
	bCanMove = false;
	GetMesh()->SetCollisionProfileName(TEXT("Ragdoll"));
	GetMesh()->SetAllBodiesSimulatePhysics(true);
	
}

void ADoctorBase::OnPawnStopMovement_Implementation()
{	
}

