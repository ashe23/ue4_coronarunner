// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class CoronaRunnerLoadingScreen : ModuleRules
{
    public CoronaRunnerLoadingScreen(ReadOnlyTargetRules Target) : base(Target)
    {
    	PrivatePCHHeaderFile = "Public/CoronaRunnerLoadingScreen.h";

        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PrivateIncludePaths.Add("CoronaRunnerLoadingScreen/Private");

        PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "Engine",
            });

        PrivateDependencyModuleNames.AddRange(
            new string[] {
                "MoviePlayer",
                "Slate",
                "SlateCore",
                "InputCore"
            }
        );
    }
}