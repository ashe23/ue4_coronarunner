// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Runtime/Core/Public/Modules/ModuleInterface.h"
#include "Modules/ModuleManager.h"

class ICoronaRunnerLoadingScreenModule : public IModuleInterface
{
public:
	static inline ICoronaRunnerLoadingScreenModule& Get()
	{
		return FModuleManager::LoadModuleChecked<ICoronaRunnerLoadingScreenModule>("CoronaRunnerLoadingScreen");
	}

	/** Kicks off the loading screen for in game loading (not startup) */
	virtual void StartInGameLoadingScreen(bool bPlayUntilStopped, float PlayTime) = 0;

	/** Stops the loading screen */
	virtual void StopInGameLoadingScreen() = 0;
};