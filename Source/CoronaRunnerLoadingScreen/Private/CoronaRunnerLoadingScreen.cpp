// Fill out your copyright notice in the Description page of Project Settings.

#include "CoronaRunnerLoadingScreen.h"
#include "SlateBasics.h"
#include "SlateExtras.h"
#include "MoviePlayer.h"
#include "Widgets/Images/SThrobber.h"
#include "Widgets/Images/SImage.h"

struct FCoronaRunnerScreenBrush : public FSlateDynamicImageBrush, public FGCObject
{
	FCoronaRunnerScreenBrush(const FName InTextureName, const FVector2D& InImageSize)
		: FSlateDynamicImageBrush(InTextureName, InImageSize)
	{
		SetResourceObject(LoadObject<UObject>(NULL, *InTextureName.ToString()));
	}

	virtual void AddReferencedObjects(FReferenceCollector& Collector)
	{
		if (UObject* CachedResourceObject = GetResourceObject())
		{
			Collector.AddReferencedObject(CachedResourceObject);
		}
	}
};


class SCoronaRunnnerLoadingScreen : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SCoronaRunnnerLoadingScreen) {}
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs)
	{

		static const FName LoadingScreenName(TEXT("/Game/CoronaRunner/Widgets/T_MainMenu_BG.T_MainMenu_BG"));
		static const FName ThrobberImagePath(TEXT("/Game/CoronaRunner/Slate/Images/T_Throbber.T_Throbber"));

		// todo:ashe23 fix this hard coded size must be responsible
		LoadingScreenBrush = MakeShareable(new FCoronaRunnerScreenBrush(LoadingScreenName, FVector2D{ 1080, 1920 }));
		ThrobberImage = MakeShareable(new FCoronaRunnerScreenBrush(ThrobberImagePath, FVector2D{ 32	, 32 }));

		FSlateColor TextColor{ FLinearColor{1.0f , 0.672443f, 0.23074f} };
		FString FontPath = TEXT("CoronaRunner/Slate/Fonts/Aleo-Bold.ttf");
		FText LoadingText = FText::FromString(TEXT("Loading"));
		float FontSize = 40.0f;

		ChildSlot
			[
			SNew(SOverlay)				
			+ SOverlay::Slot()
			.HAlign(HAlign_Fill)
			.VAlign(VAlign_Fill)
			[
				SNew(SImage)
				.Image(LoadingScreenBrush.Get())
			]
			+ SOverlay::Slot()
			.HAlign(HAlign_Fill)
			.VAlign(VAlign_Fill)
			[
				SNew(SHorizontalBox)				
				+ SHorizontalBox::Slot()
				.HAlign(HAlign_Right)
				.VAlign(VAlign_Center)
				.Padding(FMargin{10.0f})
				[
					SNew(STextBlock)
					.Justification(ETextJustify::Center)
					.Font(FSlateFontInfo(FPaths::ProjectContentDir() / FontPath, FontSize))
					.ColorAndOpacity(TextColor)
					.Text(LoadingText)
					.Visibility(this, &SCoronaRunnnerLoadingScreen::GetLoadIndicatorVisibility)
				]
				+ SHorizontalBox::Slot()
				.HAlign(HAlign_Left)
				.VAlign(VAlign_Center)
				.Padding(0.0f , 18.0f, 0.0f , 0.0f)
				[
					SNew(SThrobber)
					.NumPieces(3)
					.PieceImage(ThrobberImage.Get())
					.Visibility(this, &SCoronaRunnnerLoadingScreen::GetLoadIndicatorVisibility)
				]
			]			
		];
	}
private:
	EVisibility GetLoadIndicatorVisibility() const
	{
		bool Vis = GetMoviePlayer()->IsLoadingFinished();
		return Vis ? EVisibility::Collapsed : EVisibility::Visible;
	}

	TSharedPtr<FSlateDynamicImageBrush> LoadingScreenBrush;
	TSharedPtr<FSlateDynamicImageBrush> ThrobberImage;

};

class FCoronaRunnerLoadingScreenModule : public ICoronaRunnerLoadingScreenModule
{
public:
	virtual void StartupModule() override
	{
		// Force load for cooker reference
		LoadObject<UObject>(nullptr, TEXT("/Game/CoronaRunner/Widgets/T_MainMenu_BG.T_MainMenu_BG"));
		LoadObject<UObject>(nullptr, TEXT("/Game/CoronaRunner/Slate/Images/T_Throbber.T_Throbber"));

		if (IsMoviePlayerEnabled())
		{
			CreateScreen();
		}

	}
	
	virtual bool IsGameModule() const override
	{
		return true;
	}

	virtual void StartInGameLoadingScreen(bool bPlayUntilStopped, float PlayTime)
	{

		FLoadingScreenAttributes LoadingScreen;
		LoadingScreen.bAutoCompleteWhenLoadingCompletes = !bPlayUntilStopped;
		LoadingScreen.bWaitForManualStop = bPlayUntilStopped;
		LoadingScreen.bAllowEngineTick = bPlayUntilStopped;
		LoadingScreen.MinimumLoadingScreenDisplayTime = PlayTime;
		LoadingScreen.WidgetLoadingScreen = SNew(SCoronaRunnnerLoadingScreen);
		GetMoviePlayer()->SetupLoadingScreen(LoadingScreen);
	}

	virtual void StopInGameLoadingScreen()
	{
		GetMoviePlayer()->StopMovie();
	}

	virtual void CreateScreen()
	{
		FLoadingScreenAttributes LoadingScreen;
		LoadingScreen.bAutoCompleteWhenLoadingCompletes = true;
		LoadingScreen.MinimumLoadingScreenDisplayTime = 3.f;
		LoadingScreen.WidgetLoadingScreen = SNew(SCoronaRunnnerLoadingScreen);
		GetMoviePlayer()->SetupLoadingScreen(LoadingScreen);
	}

};

IMPLEMENT_GAME_MODULE(FCoronaRunnerLoadingScreenModule, CoronaRunnerLoadingScreen);