// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class UE4_CoronaRunnerEditorTarget : TargetRules
{
	public UE4_CoronaRunnerEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "UE4_CoronaRunner" ,  "CoronaRunnerLoadingScreen"} );
	}
}
